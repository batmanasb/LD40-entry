extends Control

onready var container = get_node("Panel/ScrollContainer/VBoxContainer")

func _ready():
	var avg_slope = Global.avg_slope
	
	add_label("Best lap times:")
	
	var cheating = false
	for val in Global.high_scores.values():
		if(val == 0):
			cheating = true
	
	#convert keys to ints and sort them
	var keys = Global.high_scores.keys()
	var int_keys = []
	for key in keys:
		int_keys.append(int(key))
	int_keys.sort()
	
	#write out every high score (best lap time for each vehicle)
	for key in int_keys:
		var val = Global.high_scores[str(key)]
		if(val == 0):
			add_label(str(key + 1, ") ", "skipped"))
		else:
			add_label(str(key + 1, ") ", Global.format_time(val)))
		
	add_label(str(""))
	
	if(not cheating):
		
		add_label(str("Average Slope: ", avg_slope))
		
		add_label(str(""))
		
		add_label(str("Results:\n"))
		
		if(avg_slope > 0):
			add_label(str("Hmm, it looks like the more you have, the worse it is..."))
			add_label(str(""))
			add_label(str("This concludes our testing, expect your paycheck in the mail within the month."))
		else:
			add_label(str("Good going,\n    Now this game doesn't follow the theme, all thanks to you and your poor testing practices.\nExpect this to come out of your paycheck!"))
			add_label(str(""))
			add_label(str("This concludes our testing."))
	
	else:
		add_label(str("Results were disabled due to cheating, we don't pay you to skip tests..."))

func add_label(message):
	var label = Label.new()
	label.set_text(message)
	container.add_child(label)


func _on_Button_pressed():
	Global.restart_game()
