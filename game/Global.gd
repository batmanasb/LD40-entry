extends Node

#persistant vars
var high_scores = {"0" : 0}
var level = 1
var seen_intro = false
var mute = false

#ending stats
var avg_slope = 0

const main = preload("res://Main.tscn")
const ending = preload("res://Ending.tscn")

func _ready():
	pass

func next_level(skip = false):
	level += 1 if not skip else 5
	high_scores[str(level-1)] = 0
	get_tree().change_scene_to(main)

func retry_level():
	get_tree().change_scene_to(main)

func submit_test_results():
	#convert keys to ints and sort them
	var keys = Global.high_scores.keys()
	var int_keys = []
	for key in keys:
		int_keys.append(int(key))
	int_keys.sort()
	
	#calculate avg slope
	var sum_slopes = 0
	var skipped = 0
	for i in range(int_keys.size()-1):
		sum_slopes += Global.high_scores[str(int_keys[i+1])] - Global.high_scores[str(int_keys[i])]
		avg_slope = sum_slopes/(int_keys.size()-skipped-1)
	
	get_tree().change_scene_to(ending)

func format_time(time):
	var int_time = int(time)
	var minutes = int_time / 60
	var seconds = int_time % 60
	var str_seconds = str(seconds)
	if(str_seconds.length() == 1):
		str_seconds = "0" + str_seconds
	return str(minutes,":",str_seconds)

func restart_game():
	high_scores = {"0" : 0}
	level = 1
	seen_intro = false
	retry_level()