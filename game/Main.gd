extends Node2D

onready var warning_sprite = get_node("CanvasLayer/Warning")
onready var warning_anim = get_node("CanvasLayer/Warning/AnimationPlayer")
onready var road_area = get_node("RaceTrack/RoadArea")
onready var car = get_node("Car")
onready var timer_label = get_node("CanvasLayer/Control/Timer")
onready var best_time_label = get_node("CanvasLayer/Control/BestTime")
onready var speed_label = get_node("CanvasLayer/Control/Speed")
onready var menu = get_node("CanvasLayer/Control/Panel")
onready var intro = get_node("CanvasLayer/Control/Intro")
onready var restart_button = get_node("CanvasLayer/Control/RestartButton")
onready var sound_button = get_node("CanvasLayer/Control/SoundButton")
onready var skip_button = get_node("CanvasLayer/Control/SkipButton")

var has_started = false
var has_ended = false
var check_points = 0
var timer = 0.0

func _ready():
	set_process(true)
	get_best_time()
	car.initialize_car(Global.level)
	if(not Global.seen_intro):
		intro.show()
	sound_button.set_pressed(not Global.mute)
	
	if(Global.high_scores[str(Global.level - 1)] == 0):
		skip_button.hide()

func _process(delta):
	if(not Global.seen_intro and Input.is_action_pressed("MB1")):
		Global.seen_intro = true
		intro.hide()
	
	if(has_started and not has_ended):
		timer += delta
		timer_label.set_text(Global.format_time(timer))

func flash_warning_animation():
	warning_anim.play("flash_warning")

func _on_RoadArea_body_enter( body ):
	car.set_dust_enabled(false)

func _on_RoadArea_body_exit( body ):
	car.set_dust_enabled(true)


func _on_CheckPoint1_body_enter( body ):
	if(check_points == 1):
		check_points = 2


func _on_CheckPoint2_body_enter( body ):
	if(check_points == 2):
		check_points = 3


func _on_CheckPoint3_body_enter( body ):
	if(check_points == 3):
		check_points = 4


func _on_CheckPoint4_body_enter( body ):
	if(check_points == 0):
		check_points = 1
		has_started = true
	elif(check_points == 4):
		has_ended = true
		menu.show()
		skip_button.show()
		if(timer < Global.high_scores[str(Global.level - 1)] or Global.high_scores[str(Global.level - 1)] < 1):
			Global.high_scores[str(Global.level - 1)] = timer
			best_time_label.set_text(Global.format_time(timer))


func _on_HButtonArray_button_selected( button_idx ):
	if(button_idx == 2):
		Global.next_level()
	if(button_idx == 1):
		Global.retry_level()
	if(button_idx == 0):
		Global.submit_test_results()

func get_best_time():
	best_time_label.set_text(Global.format_time(Global.high_scores[str(Global.level - 1)]))


func _on_RestartButton_pressed():
	Global.retry_level()

func show_restart_button():
	restart_button.show()

func set_speed(speed):
	speed_label.set_text(str(int(speed), " MPH"))

func _on_SkipButton_pressed():
	Global.next_level(true)

func _on_SoundButton_toggled( pressed ):
	car.mute = not pressed
	Global.mute = not pressed
	sound_button.release_focus()
