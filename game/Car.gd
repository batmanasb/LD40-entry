extends RigidBody2D

#leveling stats
var boost_per_engine = 10000
var mass_per_block = 2
var smoke_per_engine = 100
var particle_lifetime_per_engine = 0.1
var particle_initial_size_per_engine = 15
var particle_final_size_per_engine = 30
var volume_db_per_engine = 3

#velocity
var engines = 1
var vel_direction = Vector2(0, -1)
var initial_speed = boost_per_engine
var vel = vel_direction * initial_speed

#crash cooldown
var crash_cooldown = 0.3
var crash_timer = -1.0

#rotation
var initial_rot_speed = 0.1
var rot_speed = initial_rot_speed

#car size
var initial_size = 2
var car_size = initial_size + engines - 1

#particles
var max_particles = 1000
var is_dust_enabled = false

#sounds
var engine_rev_cap = 1500.0
var engine_rev_zone = 2000.0
var initial_vol = -20
var max_vol = 24
var is_noise_constant = false
var mute = false

#nodes
onready var main = get_tree().get_root().get_node("Game")
onready var smoke = get_node("Smoke")
onready var dust = get_node("Dust")
onready var audio = get_node("SamplePlayer")

func _ready():
	set_fixed_process(true)
	randomize()
	mute = Global.mute
	
func _fixed_process(delta):
	process_movement(delta)

func process_movement(delta):
	if(crash_timer < 0.0):
		#check if a crash occurred
		if(get_colliding_bodies().size() > 0):
			if(not mute):
				audio.play(str("Crash", str(randi()%3+1)), false)
			main.show_restart_button()
			crash_timer = crash_cooldown
			main.flash_warning_animation()
			smoke.set_emitting(false)
		process_user_input(delta)
	else:
		crash_timer -= delta

func process_user_input(delta):
	var speed = get_linear_velocity().length() / 50.0
	main.set_speed(speed)
	
	#emit smoke when engine is on
	if(not smoke.is_emitting()):
		smoke.set_emitting(true)
	
	#emit dust when driving
	if(not dust.is_emitting() and is_dust_enabled):
		dust.set_emitting(true)
	
	#rotate the player after the mouse smoothly
	var mouseAngle = get_global_pos().angle_to_point(get_global_mouse_pos())
	set_rot(lerp_angle(get_rot(), mouseAngle, rot_speed))
	
	#move car when holding left mouse button (Mouse Button 1)
	if(Input.is_action_pressed("MB1")):
		#direct the velocity in the direction the car is facing
		var directed_vel = vel.rotated(get_rot()) * delta
		apply_impulse(Vector2(), directed_vel)
		
		#rev engine when accelerating from slower speeds
		var speed = get_linear_velocity().length()
		if(speed < engine_rev_cap):
			if(not audio.is_active() or is_noise_constant):
				if(not mute):
					audio.play(str("Accelerate", str(randi()%7+1)), true)
				is_noise_constant = false
		else:
			if(not audio.is_active()):
				if(not mute):
					audio.play("Constant", true)
				is_noise_constant = true
	else:
		#stop emitting smoke when engine is off
		if(smoke.is_emitting()):
			smoke.set_emitting(false)
		#stop emitting dust when engine is off
		if(dust.is_emitting()):
			dust.set_emitting(false)
		
		#cut the engine sound
		if(audio.is_active() and crash_timer < 0.0):
			audio.stop_all()

func lerp_angle(a, b, t):
    if abs(a-b) >= PI:
        if a > b:
            a = normalize_angle(a) - 2.0 * PI
        else:
            b = normalize_angle(b) - 2.0 * PI
    return lerp(a, b, t)


func normalize_angle(x):
    return fposmod(x + PI, 2.0 * PI) - PI

func adjust_car():
	var tilemap_node = get_node("TileMap")
	var collision_shape = get_shape(0)
	remove_shape(0)
	
	#draw car
	tilemap_node.clear()
	var i = 0
	tilemap_node.set_cell(0, -1, 0)
	while(i < car_size - 1):
		tilemap_node.set_cell(0, i, 1)
		i += 1
	tilemap_node.set_cell(0, i, 2)
	
	#set tilemap position
#	var tilemap_pos = Vector2(-32, -32 * engines) 
	var tilemap_pos = Vector2(-32, 0) 
	tilemap_node.set_pos(tilemap_pos)
	
	#set collison size
	var shape = CapsuleShape2D.new()
	shape.set_radius(20)
	shape.set_height(64 * car_size)
	add_shape(shape, Matrix32(Vector2(1,0), Vector2(0,1), Vector2(0, 32 * car_size)))
	
	#driving settings
	var new_speed = initial_size + boost_per_engine * engines
	vel = vel_direction * new_speed
	set_mass(car_size * mass_per_block)
	
	#particle settings
	smoke.set_pos(Vector2(0, 64 * engines + 64))
	var num_particles = smoke_per_engine * engines
	if(num_particles > max_particles):
		num_particles = max_particles
	smoke.set_amount(num_particles)
	smoke.set_lifetime(particle_lifetime_per_engine * engines)
	smoke.set_param(Particles2D.PARAM_INITIAL_SIZE, particle_initial_size_per_engine * engines)
	smoke.set_param(Particles2D.PARAM_FINAL_SIZE, particle_final_size_per_engine * engines)
	
	#sounds
	var vol = initial_vol + volume_db_per_engine * engines
	if(vol > max_vol):
		vol = max_vol
	audio.set_default_volume_db(vol)

func initialize_car(level):
	engines = level
	car_size = initial_size + engines - 1
	adjust_car()

func set_dust_enabled(is_enabled):
	is_dust_enabled = is_enabled
	if(not is_enabled):
		dust.set_emitting(false)